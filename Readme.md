SugarCube 2 Sugary Sweet Switch Theme
========

Including the trio of files found in the src/ directory as passages in a
SugarCube 2 story will add a button to the sidebar that allows the reader to
toggle a light mode (drawn from
[tmedwards's bleached theme](https://www.motoslave.net/sugarcube/2/)) and the
default dark mode. As the presence or absence of a light mode is an
accesssibility issue, the use of these passages should make SugarCube 2 stories
readable for a larger number of people.

Please do keep in mind that, if you use alternate text colors designed to match
with the dark mode, you may need to account for that and provide alternatives
for when the light mode is active lest they become unreadable. This is
particularly true of pale and bright colors, so be careful.

The easiest way to handle this is by using style classes rather than direct
styles, like so:

```js
@@.warning;This is a warning.@@
```

The CSS file for the story would then contain this:

```css
:root {
  --warning-color: yellow;
}

.warning {
  color: var(--warning-color);
}
```

Finally, we need to add colors for the themes. We can do this either via the
PassageReady special passage, like so:

```js
:: PassageReady

<<script>>
  themes.dark['--warning-color'] = 'yellow'
  themes.light['--warning-color'] = 'red'
<</script>>
```

or in a JavaScript file by making use of the :passagestart event, like so:

```js
$(document).on(':passagestart', function (e) {
  themes.dark['--warning-color'] = 'yellow'
  themes.light['--warning-color'] = 'red'
})
```

Adding our new variables directly to the objects in
sugary-sweet-switch-themes.js is of course an option as well, and given that
this is now a separate file for the themes alone and unlikely to see any updates
unless the Bleached or default themes do, it is not as unwise as it was in early
versions of this addon. That said, it's still not recommended.

You can of course also use CSS variables for local styles, but using style
classes has the benefit of letting you keep a list of the ways in which you're
making use of themes rather than requiring you to trawl through your passages.


The current theme is stored in the `theme` key of the story's metadata, so it
will persist across refreshes, restarts, and suchlike. You can check its status
like so using SugarCube 2 syntax:

```js
<<if recall('theme') == 'light')>> ...
```

From JavaScript, you may check its status like so:

```js
if (State.metadata.get('theme') == 'light') ...
```

The toggle button's text defaults to the English `TOGGLE LIGHT MODE`, but you
may alter this either within the javascript passage or by setting
`l10nStrings.toggleLightMode` to a different string. The latter is the preferred
way of handling this, especially if your story has multiple language options.

While Sugary Sweet Switch Theme is currently designed only to support a quick
toggle between light and dark modes, the backend supports the addition of
more themes as desired. Future versions may include a theme menu frontend rather
than just the toggle, but for now anything like that is on the implementer. Feel
free to experiment!
