/***********************************************************************
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org>
***********************************************************************/

if (!State.metadata.has('theme')) {
  State.metadata.set('theme', 'dark')
}

if (typeof l10nStrings.toggleLightMode == 'undefined') {
  l10nStrings.toggleLightMode = 'Toggle Light Mode'
}
$('#menu-core').prepend('<li id="menu-item-lm"><a tabindex="0"></a></li>')
$('#menu-item-lm a').ariaClick(ev => {
  if (State.metadata.get('theme') == 'light') {
    setTheme('dark')
  } else {
    setTheme('light')
  }
}).text(L10n.get('toggleLightMode'))

window.setTheme = function(theme) {
  State.metadata.set('theme', theme)
  Object.keys(themes[theme]).forEach(function(key) {
    document.documentElement.style.setProperty(key, themes[theme][key]);
  })
}

window.themes = {}

$(document).on(':passagedisplay', function (e) {
  setTheme(State.metadata.get('theme'))
})
